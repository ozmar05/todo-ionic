const express = require('express');
const app = express();
const todoRoute = express.Router();
let TodoModel = require('../model/Todo');


todoRoute.route('/').get((req, res) => {
  TodoModel.find((error, todo) => {
    if (error) {
      return next(error)
    } else {
      res.json(todo)
      console.log('Todos retrieved!')
    }
  })
})


todoRoute.route('/create-todo').post((req, res, next) => {
  TodoModel.create(req.body, (err, todo) => {
    if (err) {
      return next(err)
    } else {
      res.json(todo)
      console.log('Todo created!')
    }
  })
});


todoRoute.route('/fetch-todo/:id').get((req, res) => {
  TodoModel.findById(req.params.id, (err, todo) => {
    if (err) {
      return next(err)
    } else {
      res.json(todo)
      console.log('Todo retrieved!')
    }
  })
})


todoRoute.route('/update-todo/:id').put((req, res, next) => {
  TodoModel.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (err, todo) => {
    if (err) {
      return next(err);
    } else {
      res.json(todo)
      console.log('Todo updated!')
    }
  })
})

todoRoute.route('/delete-todo/:id').delete((req, res, next) => {
  TodoModel.findByIdAndRemove(req.params.id, (error, todo) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: todo
      })
      console.log('Todo deleted!')
    }
  })
})

todoRoute.route('/delete-todos').delete((req, res, next) => {
  TodoModel.deleteMany((error, todo) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: todo
      })
      console.log('Todos deleted!')
    }
  })
})


module.exports = todoRoute;
