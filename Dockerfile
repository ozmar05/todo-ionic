FROM node:10.13.0-alpine
WORKDIR /app
COPY .. .
RUN npm install -g ionic@5
RUN npm install
EXPOSE 8100
CMD ["ionic", "serve"]
