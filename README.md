# Ionic version 5 with Angular 9

## Requisitos para iniciar la app
* Android Studio con sdk 27 y emulator
* node 10.13.0
* docker y docker-compose >= v2.4.1

## Iniciar la app ionic 5 :

### Compilar la app
* Ejecutar `npm install`
* Actualizar archivo enviroment que se encuentra `src/environments/environment.ts:7` y cambiar `192.168.3.171 `por tu ip
* Ejecutar `ionic build`
* Ejecutar `npx cap add android` si la carpeta android no existe, si existe usar `npx cap update android`
* Identificar el path dónde esta el android studio `studio.sh` para que abra la app automáticamente y lo pegamos en el archivo capacitor.config.json
  - en la propiedad `linuxAndroidStudioPath`
* Si no se logra identificar el path no hay problema lo podemos importar desde android studio
* Agregar la línea en el tag <application for AndroidManifest.xml `android:usesCleartextTraffic="true"`

### Iniciar el server con mongo
* Ejecutar `docker-compose build` para generar las imágenes
* Ejecutar `docker-compose up` para generar los contenedores e iniciarlos
  - esto levantará el app, server api y mongo db,
  - si se usa linux se podrá acceder a localhost:8100 y ver la app corriendo
* Para detener docker compose hay que ejecutar `ctrl + c`
* Para borrar los contenedores `docker-compose down`


## Notas:
### Run
* start ionic `ionic serve`

### MongoDB 5.0.6

### docker run
`docker build -t node-serve . `
`docker run -d --network=host -p 27017:27017 --name tododb mongo:5.0`
`docker run -d --network=host -p 5000:5000 --name todoserve node-serve`
`docker exec -it tododb bash`

### Build to android

* `ionic build`
* `npx cap add android`
* `npx cap update android`
* `npx cap open android`

### Build with docker-compose

docker-compose build
docker-compose up
docker-compose down

### Update Android Source
* add the next line in tag <application for AndroidManifest.xml
* `android:usesCleartextTraffic="true"`
