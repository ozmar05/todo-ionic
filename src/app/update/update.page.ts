import {Component, OnInit} from '@angular/core';

import {Router, ActivatedRoute} from '@angular/router';
import {FormGroup, FormBuilder} from '@angular/forms';
import {TodoService} from '../service/todo.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.page.html',
})

export class UpdatePage implements OnInit {

  updateTodoFg: FormGroup;
  id: any;

  constructor(
    private todoService: TodoService,
    private activatedRoute: ActivatedRoute,
    public formBuilder: FormBuilder,
    private router: Router
  ) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.updateTodoFg = this.formBuilder.group({
      name: [''],
      description: [''],
      isComplete: [false]
    });
  }

  ionViewDidEnter() {
    this.fetchTodo(this.id);
  }

  fetchTodo(id) {
    this.todoService.getTodo(id).subscribe((data) => {
      console.log(data);
      this.updateTodoFg.setValue({
        name: data.name,
        description: data.description,
        isComplete: data.isComplete
      });
    });
  }

  onSubmit() {
    if (!this.updateTodoFg.valid) {
      return false;
    } else {
      console.log(this.updateTodoFg.value);
      this.todoService.updateTodo(this.id, this.updateTodoFg.value)
        .subscribe(() => {
          this.updateTodoFg.reset();
          this.router.navigate(['/home']);
        });
    }
  }

}
