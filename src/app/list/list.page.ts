import {Component, OnInit} from '@angular/core';
import {Todo, TodoService} from '../service/todo.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
})

export class ListPage implements OnInit {
  todos: Todo[] = [];

  constructor(private todoService: TodoService, private router: Router) {
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.todoService.getTodos().subscribe((response) => {
      this.todos = response.filter(x => x.isComplete);
    });
  }

  removeTodo(todo, i) {
    if (window.confirm('Are you sure')) {
      this.todoService.deleteTodo(todo._id)
        .subscribe(() => {
            this.todos.splice(i, 1);
            console.log('Todo deleted!');
          }
        );
    }
  }

  removeTodos() {
    if (window.confirm('Are you sure')) {
      this.todoService.deleteTodos()
        .subscribe(() => {
            console.log('Todos deleted!');
            this.router.navigate(['/home']);
          }
        );
    }
  }
}
