import {Component, OnInit, NgZone} from '@angular/core';

import {Router} from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {TodoService} from '../service/todo.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
})

export class CreatePage implements OnInit {

  todoForm: FormGroup;

  constructor(private router: Router, public formBuilder: FormBuilder, private zone: NgZone,
              private todoService: TodoService) {
    this.initForm();
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.initForm();
  }

  initForm() {
    this.todoForm = this.formBuilder.group({
      name: '',
      description: '',
      isComplete: false
    });
  }

  onSubmit() {
    if (!this.todoForm.valid) {
      return false;
    } else {
      console.log(this.todoForm.value)
      this.todoService.createTodo(this.todoForm.value)
        .subscribe((response) => {
          this.zone.run(() => {
            this.todoForm.reset();
            this.router.navigate(['/home']);
          });
        });
    }
  }

}
