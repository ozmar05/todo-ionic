import {Component} from '@angular/core';
import {Todo, TodoService} from '../service/todo.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
})
export class HomePage {
  todos: Todo[] = [];

  constructor(private todoService: TodoService, private router: Router) {
    this.todos = [];
  }

  ionViewDidEnter() {
    this.todoService.getTodos().subscribe((response) => {
      this.todos = response.filter(x => !x.isComplete);
    });
  }

  add() {
    this.router.navigate(['/create']);
  }

  doneTodo(todo: Todo) {
    todo.isComplete = true;
    this.todoService.updateTodo(todo._id, todo)
      .subscribe(() => {
        this.router.navigate(['/list']);
      });
  }

  removeTodo(todo, i) {
    if (window.confirm('Are you sure')) {
      this.todoService.deleteTodo(todo._id)
        .subscribe(() => {
            this.todos.splice(i, 1);
            console.log('Todo deleted!');
          }
        );
    }
  }
}
