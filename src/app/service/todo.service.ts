import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
export class Todo {
  _id: number;
  name: string;
  description: string;
  isComplete: boolean;
}

@Injectable({
  providedIn: 'root'
})

export class TodoService {

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  apiServer = environment.apiUrl || 'http://localhost:5000/api';

  constructor(private httpClient: HttpClient) {
  }

  createTodo(todo: Todo): Observable<any> {
    return this.httpClient.post<Todo>(this.apiServer + '/create-todo', todo, this.httpOptions)
      .pipe(
        catchError(this.handleError<Todo>('Error occured'))
      );
  }

  getTodo(id): Observable<Todo> {
    return this.httpClient.get<Todo>(this.apiServer + '/fetch-todo/' + id)
      .pipe(
        tap(_ => console.log(`Todo fetched: ${id}`)),
        catchError(this.handleError<Todo>(`Get todo id=${id}`))
      );
  }

  getTodos(): Observable<Todo[]> {
    return this.httpClient.get<Todo[]>(this.apiServer)
      .pipe(
        tap(todos => console.log('Todos retrieved!')),
        catchError(this.handleError<Todo[]>('Get todo', []))
      );
  }

  updateTodo(id, todo: Todo): Observable<any> {
    return this.httpClient.put(this.apiServer + '/update-todo/' + id, todo, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Todo updated: ${id}`)),
        catchError(this.handleError<Todo[]>('Update todo'))
      );
  }

  deleteTodo(id): Observable<Todo[]> {
    return this.httpClient.delete<Todo[]>(this.apiServer + '/delete-todo/' + id, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Todo deleted: ${id}`)),
        catchError(this.handleError<Todo[]>('Delete todo'))
      );
  }

  deleteTodos(): Observable<Todo[]> {
    return this.httpClient.delete<Todo[]>(this.apiServer + '/delete-todos/', this.httpOptions)
      .pipe(
        tap(_ => console.log(`Todos deleted`)),
        catchError(this.handleError<Todo[]>('Delete todos'))
      );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

}
